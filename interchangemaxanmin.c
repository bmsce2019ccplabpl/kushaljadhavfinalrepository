#include <stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
int main()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    int a[n];
    
    printf("Enter %d numbers\n",n);
    for(int i=0;i<n;i++)
    {
        a[i]=input();
    }
    
    int min=a[0], max=a[0], pmax=0, pmin=0;
    for(int i=1;i<n;i++)
    {
        if(a[i]>max)
        {
            max=a[i];
            pmax=i;
        }
        else if(a[i]<min)
        {
            min=a[i];
            pmin=i;
        }
        else
        continue;
    }

    a[pmax]=min;
    a[pmin]=max;
    
    printf("The new array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d ",a[i]);
    }
    
    return 0;
}
