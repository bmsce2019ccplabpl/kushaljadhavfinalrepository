#include<stdio.h>
#include<math.h>
int discriminent(int a, int b, int c)
{
    int x=(b*b)-(4*a*c);
    return x;
}

int result(int d, int a,int b, int c)
{
    int n;
    if(d==0)
    n=1;
    else if(d>0)
    n=2;
    else
    n=3;
    
    float x, x1,x2,m;
    m=sqrt(d);
    
    switch(n)
    {
        case 1:
        {
           x=(-b)/(2*a);
           printf("The root for the equation %dX^2 + %dX +%d is %f\n",a,b,c,x);
           break;
        }
        case 2:
        {
           x1=((-b)+m)/(2*a);
           x2=((-b)-m)/(2*a);
           printf("The roots for the equation %dX^2 + %dX +%d are %f and %f\n",a,b,c,x1,x2);
           break;
        }
        default:
        {
           printf("The Discriminent is negative and the Roots are IMAGINARY\n");
           break;
        }
    }
    return 0;
}

int main()
{
   int a,b,c,d;
   printf("Enter the value of A in the equation Ax^2 + Bx + C\n");
   scanf("%d",&a);
   printf("Enter the value of B in the equation Ax^2 + Bx + C\n");
   scanf("%d",&b);
   printf("Enter the value of C in the equation Ax^2 + Bx + C\n");
   scanf("%d",&c);
   
   d=discriminent(a,b,c);
   result(d,a,b,c);
   
   return 0;
}
