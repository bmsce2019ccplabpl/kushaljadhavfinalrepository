#include<stdio.h>
int input()
{
   int n;
   printf("Enter any number\n");
   scanf("%d",&n);
   return n;
} 

int compute(int a)
{
   int fact=1;
   for(int i=1;i<=a;i++)
   {
       fact=fact*i;
   }
   return fact;
}

int display(int a, int b)
{
    printf("The Factorial of the number %d is %d\n",a,b);
    return 0;
}

int main()
{
   int a=input();
   int result=compute(a);
   display(a,result);
   return 0;
}
      