#include<stdio.h>

int main()
{
    int a[5][3],max_marks;
    for(int i=0;i<5;i++)
    {
        for(int j=0;j<3;j++)
        {
            printf("Enter the marks of Student %d in Subject %d\n",i+1,j+1);
            scanf("%d",&a[i][j]);
        }
    }
    for(int j=0;j<3;j++)
    {
        max_marks=a[0][j];
        for(int i=1;i<5;i++)
        {
            if(a[i][j]>max_marks)
            max_marks=a[i][j];
        }
        printf("The max marks in subject %d is %d\n",j+1,max_marks);
    }
    return 0;
}