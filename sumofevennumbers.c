#include<stdio.h>
int input()
{
   int n;
   printf("Enter the value of n\n");
   scanf("%d",&n);
   return n;
}

int compute(int a)
{
   int sum=0;
   for(int i=0; i<=a; i=i+2)
   {
      sum =sum+(i*i);
   }
   return sum;
}

void display(int a, int b)
{
   printf("The sum of %d even numbers is %d\n", a,b);
}

int main()
{
   int a=input();
   int result=compute(a);
   display(a,result);
   return 0;
}
   