#include <stdio.h>

int time(int a,int b)
{
    int c=(a*60)+b;
    return c;
}
int display(int a)
{
    printf("The total time taken in minutes is %d\n",a);
    
}
int main()
{
    int hrs, min, tmin;
    printf("Enter the time taken in Hours and Minutes\n");
    scanf("%d %d",&hrs, &min);
    tmin=time(hrs,min);
    display(tmin);
	return 0;
}
